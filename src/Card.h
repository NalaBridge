/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef CARD_H
#define CARD_H

#include "my_types.h"

#define Club	0
#define Diamond 1
#define	Heart	2
#define Spider	3

typedef enum CC
{C2= 1,C3,C4,C5,C6,C7,C8,C9,C10= 9,CJ=41,CQ,CK,CA=44}ClubCard;

typedef enum DC
{D2=11,D3,D4,D5,D6,D7,D8,D9,D10=19,DJ=45,DQ,DK,DA=48}DiamondCard;

typedef enum HC
{H2=21,H3,H4,H5,H6,H7,H8,H9,H10=29,HJ=51,HQ,HK,HA=54}HeartCard;

typedef enum SC
{S2=31,S3,S4,S5,S6,S7,S8,S9,S10=39,SJ=55,SQ,SK,SA=58}SpiderCard;

//ASCII的花色对应：ascii-2:Spider,ascii-3:Heart,ascii-4:Diamond,ascii-5:Club;

//若为将牌则权加50;

typedef struct HandCard
  {
	  UINT8 card_list[13];
  }
card,*cardptr;

typedef struct Bider
{
	UINT8 suit;
	UINT8 level;
}bider;//bider是叫牌时候的结构，包括suit和level；

#endif // END OF CARD_H;
