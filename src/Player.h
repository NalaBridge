/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PLAYER_H
#define PLAYER_H

#include "Card.h"
//#include "Game.h"

#define West	1
#define	North	2
#define East	3
#define	South	4

typedef struct Act
{
	int (*bid) (void *);
	int (*send) (void *);
	int (*eat) (void *);
	int (*regret) (void *);
}
*actptr;//玩家行为；

typedef struct PlayerClass
  {
    //属性；
    int score;
    struct HandCard *pcard;//FIXME;//此处的玩家持牌card 需要与 出牌时候的card变量 分开；
    //接口；
    struct Act act;
  }
player;


//构造函数的声明；
void PlayerClass();

//Act接口的函数声明；
int PlayerBid(bider *);
int PlayerSend(card *);
int PlayerEat(card *);
int PlayerRegret(player *);
#endif // END OF PLAYER_H;

