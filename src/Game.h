/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GAME_H
#define GAME_H

/*!
\file Game.h
\brief 关于每局游戏的所有数据结构
*/

#include "Player.h"
#include "COO.h"

#define EndGame 4
#define Begin	1

#define Full 4

//----------------------------------------------------------------------------------
/*!
\struct GameProcess
\brief (行为接口)游戏过程
 */
//----------------------------------------------------------------------------------
typedef struct GameProcess
  {
    /*!进行叫牌
    \n 函数指针，指向GameBid()*/
    int (*bid) (void);

    /*!进行一轮出牌
    \n 函数指针，指向GamePlay()*/
    int (*play) (void);

    /*!刷新比赛数据，每轮play之后需要refresh一次
    \n 函数指针，指向GameRefresh()*/
    int (*refresh) (void);

    /*!判断过程是否结束，包括游戏过程 和 叫牌过程
    \n 函数指针，指向NoEnd()*/
    int (*noend) (void);

    /*!加入player指向的玩家，如果已满，则返回错误
    \n 函数指针，指向GameJoin()*/
    int (*join)(void *,void *);

    /*!检查游戏中是否有空缺玩家，有就返回FALSE，否则返回TRUE
    \n 函数指针，指向GameIsFull()*/
    int (*isfull)();
  }
process;

//----------------------------------------------------------------------------------
/*!\struct GameClass
\brief 模拟类，一局游戏的类型
*/
//------------------------------------------------------------------------------------
typedef struct GameClass
  {
    /*!将牌*/
    UINT8 trump;
    /*!完结投票，若所有player都投票完结，则此次活动结束*/
    UINT8 end_vote;
    /*!叫牌次序*/
    PlayerList bid_order[4];
    /*!叫牌列表,未必用得到,目前考虑用一个当前玩家flag来进行clock wise的循环*/
    PlayerList bid_list[4];
    /*!出牌次序,未必用得到,目前考虑用一个当前玩家flag来进行clock wise的循环*/
    char *play_order;
    /*!北南分数总和*/
    UINT32 NS_score;
    /*!东西分数总和*/
    UINT32 EW_score;
    /*!当前该出牌的玩家*/
    struct Player *current;

    /*!游戏过程*/
    struct GameProcess run;
  }
game,*gameptr;

/*!
\struct GlobalData
\brief 将Game中的公用数据拷贝到GlobleData中，方便后继调用
*/
typedef struct GlobalData
  {
    /*!将牌*/
    UINT8 trump;
    /*!完结投票，若所有player都投票完结，则此次活动结束*/
    UINT8 end_vote;
    /*!叫牌次序*/
    PlayerList bid_order[4];
    /*!叫牌列表*/
    PlayerList bid_list[4];
    /*!出牌次序*/
    char *play_order;
    /*!北南分数总和*/
    UINT32 NS_score;
    /*!东西分数总和*/
    UINT32 EW_score;
    /*!游戏继续标志*/
    UINT8 no_quit;
    /*!当前该出牌的玩家*/
    struct Player *current;
    /*!当前玩家编号*/
    UINT8 cur_num;
  }
global_data,gd,*gdp;

//构造函数的声明；
void GameClass(game *);

//Process接口的函数声明；
int GameBid();
int GamePlay();
int GameRefresh();
int GameJoin(game *,player *);
int GameIsFull();
int NoEnd();


#endif // END OF GAME_H;
