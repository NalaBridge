/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Player.h"
#include "Card.h"

//构造函数的函数体；
void PlayerClass(player *p)
{
	puts("Init Player Class");
	//TODO
	PlayerInit(p);
	
}

//Act接口的函数体；
int PlayerBid(bider *b)
{
	puts("Player Bid");
	//TODO
}

int PlayerSend(card *c)
{
	puts("Player Send");
	//TODO
}

/** 
 * \fn int PlayerEat(card *c)
 * 
 * @param card *c 
 * 
 * @return ERROR number
 */
int PlayerEat(card *c)
{
	puts("Player Eat");
	//TODO
}

int PlayerRegret(player *p)
{
	puts("Player Regret");
	//TODO
}
