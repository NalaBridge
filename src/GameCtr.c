/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*!
\file GameCtr.h
\brief 包含Game控制的所有函数
*/

#include <stdio.h>
#include <assert.h>

#include "Game.h"
#include "COO.h"
#include "GameCtr.h"
#include "Player.h"
#include "my_types.h"

//控制函数体的实现

/**
\var GameData
\brief 全局控制变量
 */
extern global_data GameData;

/**
 * \fn int GameStart()
 * @return ERROR number
 \brief 游戏启动
\n 启动游戏过程
 */
int GameStart()
{
  C_Class GameClass g=NewClass(GameClass,g);
  C_Class PlayerClass west=NewClass(PlayerClass,west);
  C_Class PlayerClass north=NewClass(PlayerClass,north);
  C_Class PlayerClass east=NewClass(PlayerClass,east);
  C_Class PlayerClass south=NewClass(PlayerClass,south);

  /*
  GameData.current=west;//FIXME:注意考虑如何将控制权交给当前玩家，是本程序COO实现的关键之所在；
  			//FIXME:目前还有待完善；*/

  assert(g.run.bid!=NULL);
  g.run.bid();
  printf("g.run.noend()=%d\n",g.run.noend());

  while (g.run.noend()==TRUE)
    {
      puts("ok here!\n");

      GameData.end_vote++;//实际上GameData在编程中不可见，要通过方法来设置它，此处只是示例；
      g.run.play();
      //FIXME:此处应这样:
      //g.run.refresh();
    }
  return OK;
}


/**
 * \fn int GameRestart()
 * @return ERROR number
\brief 重启游戏
\n 游戏重新启动，新开一局游戏
 */
int GameRestart()
{}

/**
 * \fn int GameEnd()
 * @return ERROR number
\brief 游戏结束
\n 结束游戏，并退出
 */
int GameEnd()
{}

/**
 * \fn int GameScoreDisplay()
 * @return ERROR number
\brief 显示游戏分数
\n 显示到目前为止游戏进度各个玩家的分数
 */
int GameScoreDisplay()
{}

/**
 * \fn int NoQuit()
 * @return ERROR number
\brief 判断一局游戏是否结束
\n 通过判断玩家的 完结投票 来决定是否结束一局游戏
\n 通常如果结束的话意味着：(1)牌已经出完、(2)叫牌完成、(3)所有玩家通过胜利宣言
 */
int NoQuit()
{
  return IsEqu(GameData.no_quit,NO);
}

/**
 * \fn int GameInit(game *g)
 * @param g
 * @return ERROR number
\brief 初始化游戏数据
\n 将游戏类、玩家类的数据初始化
 */
int GameInit(game *g)
{

  //SetData(*g,Init);
  //*g={0};
  SetData(g->trump ,South);
  SetData(g->end_vote ,Begin);
  //SetData(g->bid_order,Init);
  //SetData(g->bid_list,Init);
  //FIXME;
  //此处需要改进，关于数据类型的自动识别及初始化；

  SetData(g->play_order ,Init);
  SetData(g->NS_score ,Init);
  SetData(g->EW_score ,Init);

  SetFunc(g->run.bid ,GameBid);
  SetFunc(g->run.play ,GamePlay);
  SetFunc(g->run.refresh ,GameRefresh);
  SetFunc(g->run.noend ,NoEnd);
  SetFunc(g->run.isfull ,GameIsFull);
  SetFunc(g->run.join ,GameJoin);


}
