/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*!
\file Game.c
\brief 包含Game类的所有方法
*/

#include "Game.h"
#include "COO.h"
#include "GameCtr.h"
#include "Player.h"

//构造函数体；

/**
\var GameData
\brief 全局控制变量
 */
global_data GameData;

/**
 *\fn void GameClass(GameClass *g)
\brief ThisGame的构造函数
\param g
\brief GameClass指针类型
\n 用来初始化GameClass类型
 */
void GameClass(game *g)
{
  puts("Init This Game...");

  GameInit(g);

  //TODO
}

/**
 * \fn int GameBid()
 * @return 正常为OK，异常为ERROR
\brief 一次叫牌过程
\n 整个叫牌过程，注意在退出的时候要将end_vote重新置0，以供Game使用；
 */
int GameBid()
{
  puts("Game Bid");
  //TODO
  //GameData.end_vote=Begin;
}

/**
 * \fn int GamePlay()
 * @return 正常为OK，异常为ERROR
\brief 一次出牌过程
 */
int GamePlay()
{
  puts("Game Play");
  //TODO
}

/**
 * \fn int GameRefresh()
 * @return 正常为OK，异常为ERROR
\brief 一次数据刷新，将Game类的数据存入全局数据GlobalData
 */
int GameRefresh()
{
  puts("Game Refresh");

  //TODO
}

/**
 * \fn int GameJoin(PlayerList,player)
 * @return 正常为OK，异常为FULL
\brief 加入玩家，如果满了就返回FULL，根据GameClass中的Empty来判断是否有空缺并加入
\param game
\brief 要加入的游戏
\param player
\brief 要加入的玩家
 */
int GameJoin(game *g,player *p)
{
  if (g->run.isfull())
    return Full;

  //puts("Player %s join OK!",GameData.playname[GameData.empty]);
//TODO:p join g;
  return OK;

}

/**
 * \fn int GameIsFull()
 * @return TRUE or FALSE
\brief 根据empty判断是否有空缺玩家
*/
int GameIsFull()
{
//TODO;
  return TRUE;
}

/**
 * \fn int NoEnd()
 * @return 过程结束为TRUE，过程继续为False；
\brief 判断过程是否结束，包括游戏过程 和 叫牌过程
 */
int NoEnd()
{
  if ( !IsEqu(GameData.end_vote,EndGame) )
    return TRUE;
  return FALSE;

}
