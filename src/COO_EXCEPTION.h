/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COO_EXCEPTION
#define	COO_EXCEPTION

#include "my_types.h"
#include <setjmp.h>

typedef enum ERRNUM
{PTRNULL=0x123,} err_num;

err_num errnum;//便于函数返回值作为参考；

typedef struct EXCEPTION
  {
    UINT8 value;
    err_num num;


  }
*Exception;


//Exception Handle
#define TRY	do{   Exception e;		\
		e->value=setjmp(buf);		\
		if(e->value!=OK) CACHE(e);	\
		while(0);

#define EXPECT(expr,expect_value)		\
	do{	 e->value=expr;			\
	if(e != expect_value) longjmp(buf,e);	\
	else e=OK;				\
	}while(0);
static void inline CACHE(Exception e);

static void inline CACHE(Exception e)
{
}

#endif //END OF COO_EXCEPTION;
