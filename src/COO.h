/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef COO_H
#define COO_H

#include <sys/types.h>
#include "COO_EXCEPTION.h"
//注意，类里面实际没有构造函数和析构函数，
//我在NewClass关键函数中通过执行类型同名函数来模拟这种特性；
#define C_Class		struct
#define C_Interface	struct

#define TRUE	1
#define FALSE	0

#define OK	1
#define ERROR	0

#define YES	1
#define NO	0

#define Init	0

#define SetData(attribute,value)	do{			\
					attribute=value;	\
					}while(0);
#define SetFunc(interface,func)		SetData(interface,func);

#define InitInterface(class,interface,name)	do{				\
						class->interface=name;		\
						}while(0);

#define NewClass(type,class)	({			\
				type(&class);		\
				class;			\
				});

#define DeleteClass(class)				\
				do{			\
				class->DestoryClass();	\
				}while(0);

#define NewAtrib(type,var)				\
				do{			\
				(attribute) var;	\
				(type *) var->value;	\
				var->get=attrib_get;	\
				var->set=attrib_set;	\
				}while(0);

#define IsEqu(x,y)	(x)==(y)
#define NoEqu(x,y)	(x)!=(y)
#define IsTrue(x)	(x)==(TRUE)
#define NoTrue(x)	(x)==(FALSE)

static void atrrib_get();
static void attrib_get();

//属性的实现；
typedef struct Attribute
  {
    void *value;
    void (*get)();
    void (*set)();
  }
*attribute;

static void attrib_get()
{
	
}

static void attrib_set()
{}
//C_Class type class;



#endif // END OF COO_H;
