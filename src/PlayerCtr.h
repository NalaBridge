/***************************************************************************
 *   Copyright (C) 2008 by NalaGinrut   *
 *   suimualatsuki@163.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/**此文件为player控制程序，包括player的初始化和玩家控制模块(与GUI控制相关)*/
#ifndef PLAYER_CTR_H
#define PLAYER_CTR_H

#include "Player.h"

int PlayerInit(player *);

#endif // EMD OF PLAYER_CTR_H;
