<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>Game.h</name>
    <path>/home/mitsuki/Project/minibridge/src/</path>
    <filename>Game_8h</filename>
    <class kind="struct">GameProcess</class>
    <class kind="struct">GameClass</class>
    <class kind="struct">GlobalData</class>
    <member kind="define">
      <type>#define</type>
      <name>EndGame</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>8c9ddb5e01429088df880773a3816969</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>Begin</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>bf4dae3f2f7abc55592f351b4864b703</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct GameProcess</type>
      <name>process</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>3d21b4baad13048df76bb3e44f83f9e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct GameClass</type>
      <name>game</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>c5c2cff3acc7003ded98452836741ce8</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct GameClass *</type>
      <name>gameptr</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>accd1147736ac6bf7dff5ef502e67bc3</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct GlobalData</type>
      <name>global_data</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>fed5171e2aa66314b9c0a5b893b5be0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct GlobalData</type>
      <name>gd</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>a4d7592eee693adc47fde70bee4df231</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct GlobalData *</type>
      <name>gdp</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>35968a0a0c680dcd6253836edd4a971c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>GameClass</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>782be9340cf4ee2662527dc7ba93aeee</anchor>
      <arglist>(game *)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>GameBid</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>e2d4d0d4ca389f5670d27e871dd6180e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>GamePlay</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>47ac05f0fdb679b531906c03a6c3b3d8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>GameRefresh</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>57a4668d9ef4eb1a39534719d7198d14</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>IsEnd</name>
      <anchorfile>Game_8h.html</anchorfile>
      <anchor>b17b6a93dd769f600838859f1fc68466</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>GameClass</name>
    <filename>structGameClass.html</filename>
    <member kind="variable">
      <type>UINT8</type>
      <name>trump</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>aa12fcd91969c96e9f984bae40665b6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UINT8</type>
      <name>end_vote</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>b024efd3882b990f16f3438fbf766ab6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>bid_order</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>4f0e6a614512834c469527e3e96b7889</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>bid_list</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>3d32314f9238891a898806cb4c846068</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>play_order</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>9ae6ca7e015a703d9729f58df1066f0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UINT32</type>
      <name>NS_score</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>aa5e9b2a650f0731337b0e0c2af1b195</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UINT32</type>
      <name>EW_score</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>b131b54bcd4a53a86b951bad70b180c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct Player *</type>
      <name>current</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>1e58449a0e9b6a3a784b8a4baf24cb10</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct GameProcess</type>
      <name>run</name>
      <anchorfile>structGameClass.html</anchorfile>
      <anchor>6d2d9c073f1ea34473d4bd9dad8c7462</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>GameProcess</name>
    <filename>structGameProcess.html</filename>
    <member kind="variable">
      <type>int(*</type>
      <name>bid</name>
      <anchorfile>structGameProcess.html</anchorfile>
      <anchor>7ad5354f2a6126fb832f98f41957954f</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>play</name>
      <anchorfile>structGameProcess.html</anchorfile>
      <anchor>9f2799b3ae97f9140455740c1d6416a0</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>refresh</name>
      <anchorfile>structGameProcess.html</anchorfile>
      <anchor>ef632026b964e087723046de569ffca9</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>noend</name>
      <anchorfile>structGameProcess.html</anchorfile>
      <anchor>72aa460ed962e3e983d3643975b7080d</anchor>
      <arglist>)(void)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>GlobalData</name>
    <filename>structGlobalData.html</filename>
    <member kind="variable">
      <type>UINT8</type>
      <name>trump</name>
      <anchorfile>structGlobalData.html</anchorfile>
      <anchor>aa12fcd91969c96e9f984bae40665b6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UINT8</type>
      <name>end_vote</name>
      <anchorfile>structGlobalData.html</anchorfile>
      <anchor>b024efd3882b990f16f3438fbf766ab6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>bid_order</name>
      <anchorfile>structGlobalData.html</anchorfile>
      <anchor>4f0e6a614512834c469527e3e96b7889</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>bid_list</name>
      <anchorfile>structGlobalData.html</anchorfile>
      <anchor>3d32314f9238891a898806cb4c846068</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>play_order</name>
      <anchorfile>structGlobalData.html</anchorfile>
      <anchor>9ae6ca7e015a703d9729f58df1066f0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UINT32</type>
      <name>NS_score</name>
      <anchorfile>structGlobalData.html</anchorfile>
      <anchor>aa5e9b2a650f0731337b0e0c2af1b195</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>UINT32</type>
      <name>EW_score</name>
      <anchorfile>structGlobalData.html</anchorfile>
      <anchor>b131b54bcd4a53a86b951bad70b180c1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>/home/mitsuki/Project/minibridge/</name>
    <path>/home/mitsuki/Project/minibridge/</path>
    <filename>dir_24c4d1d2cce5951c6eb85c1112979cea.html</filename>
    <dir>/home/mitsuki/Project/minibridge/src/</dir>
  </compound>
  <compound kind="dir">
    <name>/home/mitsuki/Project/minibridge/src/</name>
    <path>/home/mitsuki/Project/minibridge/src/</path>
    <filename>dir_6406003a02b214eab032ef75da377028.html</filename>
    <file>Game.h</file>
  </compound>
</tagfile>
